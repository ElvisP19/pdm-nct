package pt.isel.pdm.AndroTweet.Widget;

import pt.isel.pdm.AndroTweet.R;
import pt.isel.pdm.AndroTweet.Activity.TimelineActivity;
import pt.isel.pdm.AndroTweet.Contract.TimelineContract;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;
import android.widget.RemoteViews;

public class AndroTweetWidget extends AppWidgetProvider {

	private PendingIntent pi = null;

	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {
		super.onUpdate(context, appWidgetManager, appWidgetIds);
		Log.d("PDM", "Widget : onUpdate");
		String txt = "";

		if (pi == null) {
			Intent it = new Intent(context, TimelineActivity.class);
			it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			pi = PendingIntent.getActivity(context, 0, it, 0);
		}
		
		// Update list
		Cursor cur = context.getContentResolver().query(
				TimelineContract.CONTENT_URI, null, null, null,
				TimelineContract._ID + " DESC ");
		while (cur.moveToNext())
			txt += cur.getString(cur
					.getColumnIndex(TimelineContract.MESSAGE)) + "\n";

		
		for (int id : appWidgetIds) {
			RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
					R.layout.widget);
			remoteViews.setTextViewText(R.id.update, txt);
			remoteViews.setOnClickPendingIntent(R.id.update, pi);
			appWidgetManager.updateAppWidget(id, remoteViews);
		}
	}
	@Override
	public void onReceive(Context ctx, Intent intent) {
		Log.v("PDM",intent.getAction());
		AppWidgetManager mgr = AppWidgetManager.getInstance(ctx);
		ComponentName cn = new ComponentName(ctx, AndroTweetWidget.class);
		onUpdate(ctx,mgr,mgr.getAppWidgetIds(cn));
		super.onReceive(ctx, intent);
		return;
	}
}

package pt.isel.pdm.AndroTweet.Activity;

import pt.isel.pdm.AndroTweet.R;
import pt.isel.pdm.AndroTweet.Service.IUserInfoOperations;
import pt.isel.pdm.AndroTweet.Service.UserInfoPullService;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.widget.TextView;
import android.widget.Toast;

public class UserInfoActivity extends Activity implements ServiceConnection {

	private ProgressDialog progress = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.user_info);
		progress = new ProgressDialog(this);
		progress.setMessage(getString(R.string.retrieving_data));
		progress.show();
	}

	private Intent intent = null;

	@Override
	protected void onResume() {
		super.onResume();

		if (intent == null)
			intent = new Intent(this, UserInfoPullService.class);
		bindService(intent, this, BIND_AUTO_CREATE);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unbindService(this);
	}

	/******* Service Related Stuff *******/

	private IUserInfoOperations _userInfoOpers = null;

	@Override
	public void onServiceConnected(ComponentName name, IBinder service) {
		_userInfoOpers = IUserInfoOperations.Stub.asInterface(service);
		new GetUserInfoData().execute((Void) null);
	}

	@Override
	public void onServiceDisconnected(ComponentName name) {
		_userInfoOpers = null;
	}

	private class GetUserInfoData extends AsyncTask<Void, Void, Void> {

		private String uName;
		private int uNrMsg, uNrSubscriptions, uNrSubscribers;

		@Override
		protected Void doInBackground(Void... params) {
			try {
				if (_userInfoOpers.getUserInfo()) {

					uName = _userInfoOpers.getUserName();
					uNrMsg = _userInfoOpers.getNumberOfStatusMessages();
					uNrSubscriptions = _userInfoOpers
							.getNumberOfSubscriptions();
					uNrSubscribers = _userInfoOpers.getNumberOfSubscribers();
				}
			} catch (RemoteException e) {
				// TODO : Treat exceptions
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			progress.dismiss();
			if (uName == null || uName.isEmpty()) {
				setTextBoxes("---", "---", "---", "---");
				Toast.makeText(UserInfoActivity.this,
						getString(R.string.username_not_found),
						Toast.LENGTH_LONG).show();
			} else {
				setTextBoxes(uName, "" + uNrMsg, "" + uNrSubscriptions, ""
						+ uNrSubscribers);
			}
		}
	}

	private void setTextBoxes(String uName, String uNrMsg,
			String uNrSubscriptions, String uNrSubscribers) {
		TextView userName, nrMsg, nrSubscriptions, nrSubscribers;
		userName = (TextView) findViewById(R.id.userInfoName);
		nrMsg = (TextView) findViewById(R.id.userInfoNrMessages);
		nrSubscriptions = (TextView) findViewById(R.id.userInfoNrSubscriptions);
		nrSubscribers = (TextView) findViewById(R.id.userInfoNrSubscribers);

		userName.setText(uName);
		nrMsg.setText("" + uNrMsg);
		nrSubscriptions.setText("" + uNrSubscriptions);
		nrSubscribers.setText("" + uNrSubscribers);
	}
}

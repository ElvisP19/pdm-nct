package pt.isel.pdm.AndroTweet.Activity;

import pt.isel.pdm.AndroTweet.R;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

public class PreferencesActivity extends PreferenceActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.layout.preferences);
		PreferenceManager.setDefaultValues(this, R.xml.preferences, true);
	}
}

package pt.isel.pdm.AndroTweet.Activity;

import pt.isel.pdm.AndroTweet.R;
import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class WidgetActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.widget_install);
		Intent it = getIntent();
		if (AppWidgetManager.ACTION_APPWIDGET_CONFIGURE.equals(it.getAction())) {

			TextView textView = (TextView) findViewById(R.id.text);
			textView.setText("Widget Installed.");

			int widgetId = it.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,-1);
			Intent result = new Intent();
			result.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);
			setResult(RESULT_OK, result);
		}
	}

	public void onClickOk(View v) {
		finish();
	}
}

package pt.isel.pdm.AndroTweet.Activity;

import pt.isel.pdm.AndroTweet.AndroTweetApplication;
import pt.isel.pdm.AndroTweet.R;
import pt.isel.pdm.AndroTweet.Service.StatusUploadService;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class StatusActivity extends NavigationMenuActivity implements
		OnClickListener, TextWatcher {

	private static final String ACTIVITY = "[AndroTwett][Status Activity]";
	private Intent statusIntent;
	private Button submit;
	private EditText statusText;
	private TextView statusTextCount;
	private AndroTweetApplication app;

	public StatusActivity() {
		super(R.menu.status_menu);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.status);
		app = (AndroTweetApplication) getApplication();

		// Get Views
		submit = ((Button) findViewById(R.id.statusActivitySubmitButton));
		statusText = ((EditText) findViewById(R.id.statusActivityTextArea));
		statusTextCount = ((TextView) findViewById(R.id.TextCount));

		// Set Count Default Values
		statusTextCount.setText(Integer.toString(app.getMaxPostMessageSize()));
		statusText.setHint(getString(R.string.statusActivityTextArea,
				app.getMaxPostMessageSize()));

		// Set Listener
		submit.setOnClickListener(this);
		statusText.addTextChangedListener(this);

		// Service
		statusIntent = new Intent(this, StatusUploadService.class);
		Log.d(ACTIVITY, "onCreate");
	}

	@Override
	protected void onResume() {
		super.onResume();
		statusText.setHint(getString(R.string.statusActivityTextArea,
				app.getMaxPostMessageSize()));
		int count = app.getMaxPostMessageSize() - statusText.length();
		statusTextCount.setText(Integer.toString(count));
		Log.d(ACTIVITY, "onResume");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.status_menu, menu);
		Log.d(ACTIVITY, "onCreateOptionsMenu");
		return true;
	}

	private void clearTextBox() {
		statusText.setText("");
	}

	@Override
	public void onClick(View v) {
		statusIntent.putExtra(StatusUploadService.KEY, statusText.getText()
				.toString());
		startService(statusIntent);
		statusIntent.removeExtra("statusText");
		clearTextBox();
	}

	/******************** TextWatcher ************************/
	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		if ((s.length() + count - 1) > app.getMaxPostMessageSize()) {
			statusText.removeTextChangedListener(this);
			statusText.setText(s.subSequence(0, app.getMaxPostMessageSize()));
			statusText.setSelection(((start + count) > app
					.getMaxPostMessageSize()) ? statusText.length() : start
					+ count);
			statusText.addTextChangedListener(this);
			Log.d(ACTIVITY, "onTextChanged");
		}
	}

	@Override
	public void afterTextChanged(Editable s) {
		int count = app.getMaxPostMessageSize() - statusText.length();
		statusTextCount.setText(Integer.toString(count));
		Log.d(ACTIVITY, "afterTextChanged");
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
	}
}

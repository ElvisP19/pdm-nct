package pt.isel.pdm.AndroTweet.Activity;

import pt.isel.pdm.AndroTweet.AndroTweetApplication;
import pt.isel.pdm.AndroTweet.R;
import pt.isel.pdm.AndroTweet.Contract.TimelineContract;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class TimelineActivity extends NavigationMenuActivity implements
		OnItemClickListener {

	private static final String ACTIVITY = "[AndroTwett][Timeline Activity]";
	private static final String CURSOR = ACTIVITY+"[CURSOR ADAPTER]";
	public ListView _list;
	private Cursor _cursor;
	private AndroTweetApplication app;
	private StatusAdapter _adapter;
	
	public TimelineActivity(){
		super(R.menu.timeline_menu);
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list_timeline);
		app = (AndroTweetApplication) getApplication();
        setContentView(R.layout.list_timeline);
        app.attachTimelineActivity(this);

		_list = (ListView) findViewById(android.R.id.list);
        
        _list.setOnItemClickListener(this);
        registerForContextMenu(_list);
        
        Log.d("PDM","Trying get cursor...");
        _cursor = getData(this);
        
        Log.d("PDM","get cursor sucessfully!");
        
        _adapter = new StatusAdapter(_cursor); 
        _list.setAdapter(_adapter);
	}
	
	public void DoUpdate(final Context ctx){
    	runOnUiThread(new Runnable() {
    	    public void run() {
    	    	_cursor = getData(ctx);
    	    	Log.d("PDM","Count " + _cursor.getCount());
    	    	_adapter.changeCursor(_cursor);
    			_adapter.notifyDataSetChanged();
    	    }
    	});
    }
    
	// -------------------------- DETAIL ------------------------------------
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		Holder h = (Holder) view.getTag();
		Intent detail = new Intent(this, DetailActivity.class);
		detail.putExtra("idmessage", h.id);
		String msg = h.msg.getText()+"";
		h.msg.setText(msg.replace(getResources().getString(pt.isel.pdm.AndroTweet.R.string.tagnewmsg), ""));
		startActivity(detail);
	}

	// -------------------------- STATUS ADAPTER
	// ------------------------------------
	public class StatusAdapter extends CursorAdapter {
		
		public StatusAdapter(Cursor cur) {
			super(TimelineActivity.this, cur);
		}
		
		@Override
		public void bindView(View v/*Existing view*/, Context context/*Interface to application's global information*/,
				Cursor cursor/*The cursor is already moved to the correct position*/) {
			Log.d("PDM",CURSOR+"[Method bindView]");
			/**Bind an existing view to the data pointed to by cursor*/
			Holder h;
			h = (Holder) v.getTag();
			
			ChangeHolder(v,h,cursor);
		}
		@Override
		public View newView(Context context/*Interface to application's global information*/, Cursor cursor/*The cursor is already moved to the correct position*/,
							ViewGroup parent/*The parent to which the new view is attached to*/) {
			Log.d("PDM",CURSOR+"[Method newView]");
			/**Makes a new view to hold the data pointed to by cursor*/
			View v;
			Holder h;
			v = getLayoutInflater().inflate(R.layout.timeline, null);
			h = new Holder();
			h.author = (TextView) v.findViewById(R.id.authortimeline);
			h.msg = (TextView) v.findViewById(R.id.messagetimeline);
			h.date = (TextView) v.findViewById(R.id.datetimeline);
			ChangeHolder(v,h,cursor);
			v.setTag(h);
			return v;
		}
		
		private void ChangeHolder(View v, Holder h, Cursor cursor){
			boolean b = Integer.valueOf(cursor.getString(cursor.getColumnIndex(TimelineContract.ISNEW)))==1;
			h.author.setText(cursor.getString(cursor.getColumnIndex(TimelineContract.AUTHOR)));
			String msg = (b?getResources().getString(pt.isel.pdm.AndroTweet.R.string.tagnewmsg):
				"")+cursor.getString(cursor.getColumnIndex(TimelineContract.MESSAGE));
			
			h.author.setText(cursor.getString(cursor.getColumnIndex(TimelineContract.AUTHOR)));	
			int n = app.getMaxCharTitle();
			h.msg.setText(msg.substring(0, msg.length() > n ? n : msg.length()));
			h.date.setText(DateUtils.getRelativeTimeSpanString((cursor.getLong((cursor.getColumnIndex(TimelineContract.DATE))))));
			h.id = cursor.getLong(cursor.getColumnIndex(TimelineContract._ID));
		}		
	}

	private static class Holder {
		public TextView author, msg, date;
		public long id;
	}
	
	private Cursor getData(Context ctx){
		return ctx.getContentResolver().query(TimelineContract.CONTENT_URI, null, null, null,TimelineContract._ID+" DESC");
	}
	
}

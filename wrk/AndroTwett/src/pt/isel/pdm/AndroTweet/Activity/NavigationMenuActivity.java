package pt.isel.pdm.AndroTweet.Activity;

import pt.isel.pdm.AndroTweet.R;
import pt.isel.pdm.AndroTweet.Service.TimelinePullService;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;

public class NavigationMenuActivity extends Activity {
	
	/** Defines xml (layout) to be used **/
	private int _menuId;
	
	public NavigationMenuActivity(int menuId)
	{
		super();
		_menuId = menuId;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(_menuId, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent = null;
		switch (item.getItemId()) 
		{
			case R.id.goto_preferences:
				intent = new Intent(this, PreferencesActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				startActivity(intent);
				return true;
	
			case R.id.goto_timeline:
				intent = new Intent(this, TimelineActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				startActivity(intent);
				return true;
				
			case R.id.goto_status:
				intent = new Intent(this, StatusActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				startActivity(intent);
				return true;
				
			case R.id.goto_user_info:
				intent = new Intent(this, UserInfoActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				startActivity(intent);
				return true;	
				
			case R.id.update_timeline:
				intent = new Intent(this, TimelinePullService.class);
				startService(intent);
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
}

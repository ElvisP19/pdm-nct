package pt.isel.pdm.AndroTweet.Activity;

import java.util.Date;

import pt.isel.pdm.AndroTweet.AndroTweetApplication;
import pt.isel.pdm.AndroTweet.R;
import pt.isel.pdm.AndroTweet.Contract.TimelineContract;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class DetailActivity extends NavigationMenuActivity implements OnClickListener{
	private static final String ACTIVITY = "[AndroTwett][Detail Activity]";
	private long _id;
	private String _msg, _date, _author;
	
	
	public DetailActivity() {
		super(R.menu.detail_menu);
	}
	
	private AndroTweetApplication app;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		_id = getIntent().getLongExtra("idmessage", -1);
		setContentView(R.layout.detail);
		app = (AndroTweetApplication) getApplication();
		
		Cursor cursor = app.getContentResolver().query(TimelineContract.CONTENT_URI, null, TimelineContract._ID+ "="+_id, null,null);
		Log.d("PDM",TimelineContract._ID+ "="+_id);
		if(cursor.moveToNext())
		{
			Log.d("PDM", "entrou");
			_msg = cursor.getString(cursor.getColumnIndex(TimelineContract.MESSAGE));
			_author = cursor.getString(cursor.getColumnIndex(TimelineContract.AUTHOR));
			_date = ""+new Date(cursor.getLong((cursor.getColumnIndex(TimelineContract.DATE))));
			_date = _date.substring(0,_date.lastIndexOf("GMT")-1);
			((TextView) findViewById(R.id.authordetail)).setText(_author);
			((TextView) findViewById(R.id.datedetail)).setText(_date+"");
			((TextView) findViewById(R.id.iddetail)).setText(""+_id);
			((TextView) findViewById(R.id.messagedetail)).setText(_msg);
		}
		cursor.close();
        Button updateStatusButton = (Button) findViewById(R.id.mail);
        updateStatusButton.setOnClickListener( this);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		ContentValues vals = new ContentValues();
		vals.put(TimelineContract.ISNEW, false);
		int rows = app.getContentResolver().update(TimelineContract.CONTENT_URI, vals, TimelineContract._ID +" = " + _id, null);
		Log.d("PDM", ACTIVITY+" [Rows Updated: "+rows+"]");
	}

	@Override
	public void onClick(View arg0) {
		try{
			final Intent emailIntent = new Intent(Intent.ACTION_SEND);
			emailIntent.setType("text/plain"); 
			emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Yamba"); 
			emailIntent.putExtra(Intent.EXTRA_TEXT, _msg); 
			this.startActivity(Intent.createChooser(emailIntent, "Send mail..."));
		}
		catch(android.content.ActivityNotFoundException e){ }
	}
}

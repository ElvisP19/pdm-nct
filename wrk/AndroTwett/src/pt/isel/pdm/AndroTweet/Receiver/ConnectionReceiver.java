package pt.isel.pdm.AndroTweet.Receiver;

import pt.isel.pdm.AndroTweet.AndroTweetApplication;
import pt.isel.pdm.AndroTweet.Contract.StatusContract;
import pt.isel.pdm.AndroTweet.Service.StatusUploadService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;

public class ConnectionReceiver extends BroadcastReceiver {

	private Intent statusIntent = null;

	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();
		AndroTweetApplication app = (AndroTweetApplication) context.getApplicationContext();
		if (action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
			boolean connect = !intent.getBooleanExtra(
					ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
			ConnectivityManager  cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
			if(cm!=null){
				NetworkInfo ni = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
				if(ni.getType() != ConnectivityManager.TYPE_WIFI) return;
			}
			if (connect) {
				app.startAlarm();
				if (statusIntent == null)
					statusIntent = new Intent(context,
							StatusUploadService.class);

				Cursor cursor = context.getContentResolver().query(
						StatusContract.CONTENT_URI, null, null, null,
						StatusContract.POST);

				while (cursor.moveToNext()) {
					// Get post
					int id = cursor.getInt(0);
					String post = cursor.getString(1);

					// Submit post
					submitPost(context, post);

					// Delete post
					context.getContentResolver().delete(
							Uri.withAppendedPath(StatusContract.CONTENT_URI, ""
									+ id), null, null);
				}
				cursor.close();
			}else app.stopAlarm();
		}
	}

	private void submitPost(Context context, String post) {
		statusIntent.putExtra(StatusUploadService.KEY, post);
		context.startService(statusIntent);
		statusIntent.removeExtra(StatusUploadService.KEY);
	}
}

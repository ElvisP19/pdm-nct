package pt.isel.pdm.AndroTweet;

import pt.isel.pdm.AndroTweet.Activity.PreferencesActivity;
import pt.isel.pdm.AndroTweet.Activity.TimelineActivity;
import pt.isel.pdm.AndroTweet.Service.TimelinePullService;
import winterwell.jtwitter.Twitter;
import android.R;
import android.app.AlarmManager;
import android.app.Application;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.widget.Button;

public class AndroTweetApplication extends Application implements
		OnSharedPreferenceChangeListener {

	// Temporary Variables. TODO: Delete after testing
	private static final String MPASSWORD = "";
	private static final String USERNAME = "";
	private static final int DEFAULT_NUMBER_CHARACTERS_TO_SHOW = 20;
	private static final String HTTP_YAMBA_MARAKANA_COM_API = "http://yamba.marakana.com/api";
	private static final int MAX_CHAR_WRITTEN = 140;
	private static final String DEFAULT_PERIODICAL_TIME = 60*1000+"";
	private static final boolean DEFAULT_CHOICE_PERIODICAL_UPDATE = true;
	private static final int DEFAULT_MAX_MESSAGES_TO_TIMELINE = 20;
	
	private Twitter twitter;
	private SharedPreferences preferences;
	
	private TimelineActivity _timelineActivity;

	public void attachTimelineActivity(TimelineActivity t){ _timelineActivity = t; }

	public void updateMenuTimeline(){
		if (_timelineActivity != null)
			_timelineActivity.DoUpdate(_timelineActivity);
	}
	
	public Button lastStatusUpdateSumitButton;
	public String selectedUser;
	
	private PendingIntent pi;
	private AlarmManager am;
	private boolean isOn = false;

	@Override
	public void onCreate() {
		super.onCreate();
		this.preferences = PreferenceManager.getDefaultSharedPreferences(this);
		this.preferences.registerOnSharedPreferenceChangeListener(this);
		Intent intent = new Intent(this,TimelinePullService.class);
		pi = PendingIntent.getService(this, 0, intent, 0);
		am = (AlarmManager) getSystemService(ALARM_SERVICE);
		this.startAlarm();
	}

	public Twitter getTwitter() {
		if (this.twitter == null) {
			String username = getUsername();
			String password = getPassword();
			String url = getUrl();
			if (!TextUtils.isEmpty(username) && !TextUtils.isEmpty(password)
					&& !TextUtils.isEmpty(url)) {
				this.twitter = new Twitter(username, password);
				this.twitter.setAPIRootUrl(url);
			}
			else{
				Intent i = new Intent(this,PreferencesActivity.class);
				i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	            startActivity(i);
			}
		}
		
		return this.twitter;
	}

	public int getMaxTimeOfPeriodicalUpdate(){
		return Integer.parseInt(preferences.getString("periodicalUpdate", DEFAULT_PERIODICAL_TIME));
	}
	
	public boolean hasPeriodicalUpdate(){
		return preferences.getBoolean("hasPeriodicalUpdate", DEFAULT_CHOICE_PERIODICAL_UPDATE);
	}
	
	public int getMaxCharTitle() {
		return Integer.parseInt(preferences.getString("maxCharTitle",
				DEFAULT_NUMBER_CHARACTERS_TO_SHOW + ""));
	}

	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		twitter = null;
	}

	public int getMaxPostMessageSize() {
		return Integer.parseInt(preferences.getString("maxChar", ""
				+ MAX_CHAR_WRITTEN));
	}

	public int getMaxMessagesToTimeline(){
		return Integer.parseInt(preferences.getString("maxMsg", ""
				+ DEFAULT_MAX_MESSAGES_TO_TIMELINE));
	}
	
	public String getUsername() {
		return preferences.getString("user", USERNAME);
	}

	public String getPassword() {
		return preferences.getString("pass", MPASSWORD);
	}

	public String getUrl() {
		return preferences.getString("url", HTTP_YAMBA_MARAKANA_COM_API);
	}
	
	public boolean hasConnection()
	{
		// Check for connectivity
		ConnectivityManager conManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		return conManager.getActiveNetworkInfo() != null
				&& conManager.getActiveNetworkInfo().isConnectedOrConnecting();
	}
	
	public void postNotification(CharSequence message, @SuppressWarnings("rawtypes") Class cls)
	{
		String ns = Context.NOTIFICATION_SERVICE;
		NotificationManager mNotificationManager = (NotificationManager) getSystemService(ns);
		int icon = R.drawable.ic_notification_overlay;
		Resources res = getResources();
		CharSequence tickerText = res.getString(pt.isel.pdm.AndroTweet.R.string.msg);
		long when = System.currentTimeMillis();

		Notification notification = new Notification(icon, tickerText, when);
		Context context = getApplicationContext();
		CharSequence contentTitle = message;
		CharSequence contentText = message;
		Intent notificationIntent = new Intent(this, cls);
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
		
		notification.setLatestEventInfo(context, contentTitle, contentText, contentIntent);
		mNotificationManager.notify(1, notification);
	}
	
	
	public void startAlarm(){
		if(!isOn){
			long tm = getMaxTimeOfPeriodicalUpdate();
			am.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()+tm, tm, pi);
			isOn = true;
		}
	}
	
	public void stopAlarm(){ if(isOn){ am.cancel(pi); isOn = false; }}
	
}

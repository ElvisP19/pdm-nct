package pt.isel.pdm.AndroTweet.Util;

import android.util.Log;

public class Utils {

	private final static String TAG = "[AndroTweet]";

	public static void log(String txt) {
		Log.v(TAG, "Activity pid=" + android.os.Process.myPid() + " thr="
				+ Thread.currentThread().getId() + ": " + txt);
	}
}

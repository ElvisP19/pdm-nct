package pt.isel.pdm.AndroTweet.Helper;

import pt.isel.pdm.AndroTweet.Contract.StatusContract;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class StatusHelper extends SQLiteOpenHelper {

	static final String TAG = "PDM";
	static final String DB_NAME = "status.db";
	static final int DB_VERSION = 1;

	public StatusHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String columns = StatusContract._ID
				+ " integer primary key autoincrement, " + StatusContract.POST
				+ " text not null";
		String sql = "CREATE TABLE " + StatusContract.TABLE + "( " + columns
				+ " )";
		db.execSQL(sql);
		Log.d(TAG, "sql= " + sql);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE if exists " + StatusContract.TABLE);
		Log.d(TAG, "onUpdated");
		onCreate(db);
	}
}

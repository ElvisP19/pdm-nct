package pt.isel.pdm.AndroTweet.Helper;

import pt.isel.pdm.AndroTweet.Contract.TimelineContract;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class TimelineHelper extends SQLiteOpenHelper{

	static final String TAG = "PDM";
	static final String DB_NAME = "timeline.db";
	static final int DB_VERSION = 1;

	public TimelineHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE if exists " + TimelineContract.TABLE);
		Log.d(TAG, "onUpdated");
		onCreate(db);
	}


	@Override
	public void onCreate(SQLiteDatabase db) {
		String columns = TimelineContract._ID + " long primary key, "
			       + TimelineContract.MESSAGE + " text not null, "
			       + TimelineContract.AUTHOR + " text not null, "
			       + TimelineContract.DATE + " long not null,"
			       + TimelineContract.ISNEW + " boolean not null";
			       //+ TimelineContract.ID_POST + " long not null";
		String sql = "CREATE TABLE "+ TimelineContract.TABLE + "( "+ columns + " )";
		db.execSQL(sql);
		Log.d(TAG, "sql= " + sql);
	}
}

package pt.isel.pdm.AndroTweet.Service;

import pt.isel.pdm.AndroTweet.AndroTweetApplication;
import pt.isel.pdm.AndroTweet.Util.Utils;
import winterwell.jtwitter.Twitter;
import winterwell.jtwitter.Twitter.User;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

public class UserInfoPullService extends Service {

	public class UserInfoOperations extends IUserInfoOperations.Stub {

		private User _user;
		private static final String SERVICE = "[AndroTwett][UserInfo Service]";
		@Override
		public boolean getUserInfo() throws RemoteException {
			try {
				AndroTweetApplication app = (AndroTweetApplication) getApplication();
				Twitter t = app.getTwitter();
				if (t == null)
				{
					Log.e(SERVICE, "Objecto Twitter � null. vai sair");
					return false;
				}
				_user = t.getUser(app.selectedUser);
				return true;
			} catch (RuntimeException re) {
				Utils.log(re.getMessage());
				return false;
			}
		}

		@Override
		public String getUserName() throws RemoteException {
			return _user != null ? _user.getName() : null;
		}

		@Override
		public byte[] getProfileImage() throws RemoteException {
			throw new UnsupportedOperationException();
		}

		@Override
		public int getNumberOfStatusMessages() throws RemoteException {
			return _user != null ? _user.getStatusesCount() : -1;
		}

		@Override
		public int getNumberOfSubscriptions() throws RemoteException {
			return _user != null ? _user.getFriendsCount() : -1;
		}

		@Override
		public int getNumberOfSubscribers() throws RemoteException {
			return _user != null ? _user.getFollowersCount() : -1;
		}

	}

	IBinder opers = new UserInfoOperations();

	@Override
	public IBinder onBind(Intent arg0) {
		return opers;
	}
}

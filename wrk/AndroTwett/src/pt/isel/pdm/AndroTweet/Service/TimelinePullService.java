package pt.isel.pdm.AndroTweet.Service;

import java.util.List;

import pt.isel.pdm.AndroTweet.AndroTweetApplication;
import pt.isel.pdm.AndroTweet.Activity.TimelineActivity;
import pt.isel.pdm.AndroTweet.Contract.TimelineContract;
import pt.isel.pdm.AndroTweet.Widget.AndroTweetWidget;
import winterwell.jtwitter.Twitter;
import winterwell.jtwitter.Twitter.Status;
import android.app.IntentService;
import android.appwidget.AppWidgetManager;
import android.content.ContentValues;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.util.Log;

public class TimelinePullService extends IntentService {

	public static final String KEY = "TIMELINE";
	private static final String SERVICE = "[AndroTwett][Timeline Service]";
	private Intent notifyWidgetIntent = null;
	
	public TimelinePullService() {
		super("TimelinePullService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		AndroTweetApplication app = (AndroTweetApplication) getApplication();
		boolean connected = app.hasConnection();
		Log.d("PDM", "on timelinepull service");
		if (connected) {
			if(!app.hasPeriodicalUpdate())return;
			try {
				int newMessages = 0;
				int maxMessages = app.getMaxMessagesToTimeline();
				int elements = getNumberMessagesOnDB();
				Log.d("PDM","Num MSG: "+elements);
				Twitter t = app.getTwitter();
				if (t == null)
				{
					Log.e(SERVICE, "Objecto Twitter � null. vai sair");
					return;
				}
				List<Twitter.Status> timeline = t.getUserTimeline();
				int msg = 0;
				for (Status s : timeline) {
					if ((msg++) == maxMessages) break;
					Cursor cursor = app.getContentResolver().query(
							TimelineContract.CONTENT_URI, null,
							TimelineContract._ID + "=" + s.getId(), null, null);
					if (!cursor.moveToNext()) {
						ContentValues vals = getValues(s.getUser().getName(), s.getCreatedAt().getTime(), s.getId(),s.getText());						
						if (getContentResolver().insert(TimelineContract.CONTENT_URI, vals) != null){
							++newMessages;
							Log.d("PDM", "NEW");
						}
					}
					Log.d("PDM", "STATUS ID:" + s.getId());
					cursor.close();
				}
				Log.d(SERVICE, newMessages + " messages");
				int msgtoremove = elements+newMessages-maxMessages;
				if (msgtoremove > 0) {
					Cursor toremove = app.getContentResolver().query(
							TimelineContract.CONTENT_URI, null, null, null,
							TimelineContract._ID + " ASC");
					while(toremove.moveToNext()){
						if(msgtoremove--==0)break;
						getContentResolver().delete(TimelineContract.CONTENT_URI,
							TimelineContract._ID+" = "+ toremove.getLong(toremove.getColumnIndex(TimelineContract._ID)),null);
					}
					toremove.close();
				}
				if(newMessages > 0){
					Resources res = getResources();
					((AndroTweetApplication) getApplication()).postNotification(
							res.getString(pt.isel.pdm.AndroTweet.R.string.thereis)+" "+ newMessages+ ((newMessages == 1) ? 
									res.getString(pt.isel.pdm.AndroTweet.R.string.msg) + res.getString(pt.isel.pdm.AndroTweet.R.string.onenew)
									: res.getString(pt.isel.pdm.AndroTweet.R.string.msgs) + res.getString(pt.isel.pdm.AndroTweet.R.string.morenew)),
							TimelineActivity.class);
					app.updateMenuTimeline();
//					
					// Update Widget
//					if (notifyWidgetIntent == null) {
						Log.d("PDM","on if");
						notifyWidgetIntent = new Intent(this,
								AndroTweetWidget.class);
//						notifyWidgetIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						notifyWidgetIntent
								.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
//					}
					sendBroadcast(notifyWidgetIntent);
					Log.d("PDM",notifyWidgetIntent+"");
				}
				

				
			} catch (Exception ex) {
				Log.e(SERVICE, "Timeline Process Exception " + ex);
			}
		}
	}
	
	private int getNumberMessagesOnDB(){
		Cursor c = ((AndroTweetApplication) getApplication()).getContentResolver().query(TimelineContract.CONTENT_URI, null,null, null, null);
		int elements = c.getCount();
		c.close();
		return elements;
	}
	
	private ContentValues getValues(String name, long date, long id, String txt){
		ContentValues vals = new ContentValues();
		vals.put(TimelineContract.AUTHOR, name);
		vals.put(TimelineContract.DATE, date);
		vals.put(TimelineContract.MESSAGE, txt);
		vals.put(TimelineContract._ID, id);
		vals.put(TimelineContract.ISNEW, true);
		return vals;
	}
}

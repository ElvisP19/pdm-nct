package pt.isel.pdm.AndroTweet.Service;

import pt.isel.pdm.AndroTweet.AndroTweetApplication;
import pt.isel.pdm.AndroTweet.Activity.StatusActivity;
import pt.isel.pdm.AndroTweet.Contract.StatusContract;
import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.content.res.Resources;
import android.util.Log;

public class StatusUploadService extends IntentService {

	public static final String KEY = "statusMessage";
	private static final String SERVICE = "[AndroTwett][Status Service]";

	public StatusUploadService() {
		super("StatusUploadService");
	}

	protected void onHandleIntent(Intent intent) {
		String post = intent.getStringExtra(KEY);
		AndroTweetApplication app =((AndroTweetApplication) getApplication()); 
		boolean connected = app.hasConnection();
		if (!connected) { // Does not have connectivity push to queue
			ContentValues vals = new ContentValues();
			vals.put(StatusContract.POST, post);
			getContentResolver().insert(StatusContract.CONTENT_URI, vals);
		} else { // Has connectivity dispatch to server
			try {
				String x = app.getTwitter().updateStatus(post).text;
				if (x == null){
					Log.e(SERVICE, "Objecto Twitter � null. vai sair");
					return;
				}
				//TODO: Send Notification to Status Bar
				Resources res = getResources();
				Log.d(SERVICE, res.getString(pt.isel.pdm.AndroTweet.R.string.msg) + x);
				((AndroTweetApplication)getApplication()).postNotification(res.getString(pt.isel.pdm.AndroTweet.R.string.receivemsg), 
						StatusActivity.class);
			} catch (Exception ex) {
				Log.e(SERVICE, "Submission Exception");
			}
		}
		Log.d(SERVICE, "onClick");
	}
}

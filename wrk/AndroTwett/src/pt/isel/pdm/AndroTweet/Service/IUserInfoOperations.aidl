package pt.isel.pdm.AndroTweet.Service;

interface IUserInfoOperations {
	
	boolean getUserInfo();
	String getUserName();
	byte[] getProfileImage();
	int getNumberOfStatusMessages();
	int getNumberOfSubscriptions();
	int getNumberOfSubscribers();
}	
	
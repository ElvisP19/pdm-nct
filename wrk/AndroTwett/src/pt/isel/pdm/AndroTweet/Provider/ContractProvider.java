package pt.isel.pdm.AndroTweet.Provider;

import pt.isel.pdm.AndroTweet.Contract.StatusContract;
import pt.isel.pdm.AndroTweet.Contract.TimelineContract;
import pt.isel.pdm.AndroTweet.Helper.StatusHelper;
import pt.isel.pdm.AndroTweet.Helper.TimelineHelper;
import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

public class ContractProvider extends ContentProvider {

	public static final String AUTHORITY = "pt.isel.pdm.AndroTweet.Provider";
	public static final Uri CONTENT_URI = Uri
			.parse(ContentResolver.SCHEME_CONTENT + "://" + AUTHORITY);

	private TimelineHelper timelineHelper;
	private StatusHelper statusHelper;

	private static final int TIMELINE_ALL = 1;
	private static final int TIMELINE_ID = 2;
	private static final int STATUS_ALL = 1;
	private static final int STATUS_ID = 2;
	

	private static final UriMatcher uriMatcher = new UriMatcher(
			UriMatcher.NO_MATCH);

	static {
		uriMatcher.addURI(AUTHORITY, "TIMELINE", TIMELINE_ALL);
		uriMatcher.addURI(AUTHORITY, "TIMELINE/#", TIMELINE_ID);
		uriMatcher.addURI(AUTHORITY, "STATUS", STATUS_ALL);
		uriMatcher.addURI(AUTHORITY, "STATUS/#", STATUS_ID);
	}

	@Override
	public boolean onCreate() {
		Log.v("PDM", "TimelineProvider: onCreate()");
		timelineHelper = new TimelineHelper(getContext());
		statusHelper = new StatusHelper(getContext());
		return true;
	}

	/************************* CRUD ************************************/
	@Override
	public Uri insert(Uri uri, ContentValues values) {
		SQLiteDatabase db = getWritableDatabase(uri);//timelineHelper.getWritableDatabase();
		try {
			long row = db.insert(getContract(uri), null, values);
			return (row == -1) ? null : ContentUris.withAppendedId(uri, row);
		} finally {
			db.close();
		}
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		SQLiteDatabase db = getWritableDatabase(uri);timelineHelper.getWritableDatabase();
		try {
//			selection = makeSelection(uri, selection);
			return db.delete(getContract(uri), selection, selectionArgs);
		} finally {
			db.close();
		}
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
//		selection = makeSelection(uri, selection);
		SQLiteDatabase db = getReadableDatabase(uri);//timelineHelper.getReadableDatabase();
		return db.query(getContract(uri), projection, selection,
				selectionArgs, null, null, sortOrder);
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] args) {
//		selection = makeSelection(uri, selection);
		SQLiteDatabase db = getWritableDatabase(uri);//timelineHelper.getWritableDatabase();
		return db.update(getContract(uri), values, selection, args);
	}

	/************************* MIME ************************************/

	private static final String MIME_TIMELINE_ALL = "vnd.android.cursor.dir/vnd.isel.pdm.TIMELINE";
	private static final String MIME_TIMELINE_ONE = "vnd.android.cursor.item/vnd.isel.pdm.TIMELINE";
	
	private static final String MIME_STATUS_ALL = "vnd.android.cursor.dir/vnd.isel.pdm.STATUS";
	private static final String MIME_STATUS_ONE = "vnd.android.cursor.item/vnd.isel.pdm.STATUS";

	@Override
	public String getType(Uri uri) {
		if(uriMatcher.match(uri) == STATUS_ALL) return MIME_STATUS_ALL;
		if(uriMatcher.match(uri) == STATUS_ID) return MIME_STATUS_ONE;
		return uriMatcher.match(uri) == TIMELINE_ALL ? MIME_TIMELINE_ALL
				: MIME_TIMELINE_ONE;
	}

	private String getContract(Uri uri){
		if(uri.toString().equals(StatusContract.CONTENT_URI.toString())) return StatusContract.TABLE;
		return TimelineContract.TABLE;
	}
	
	private SQLiteDatabase getWritableDatabase(Uri uri){
		if(uri.toString().equals(StatusContract.CONTENT_URI.toString())) return statusHelper.getWritableDatabase();
		return timelineHelper.getWritableDatabase();
	}
	
	private SQLiteDatabase getReadableDatabase(Uri uri){
		if(uri.toString().equals(StatusContract.CONTENT_URI.toString())) return statusHelper.getReadableDatabase();
		return timelineHelper.getReadableDatabase();
	}
	
//	private String makeSelection(Uri uri, String sel) {
//		switch (uriMatcher.match(uri)) {
//		case TIMELINE_ID:
//			return TimelineContract._ID + "=" + uri.getLastPathSegment();
//		default:
//			return sel;
//		}
//	}
}

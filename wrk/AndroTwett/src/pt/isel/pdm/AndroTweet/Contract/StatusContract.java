package pt.isel.pdm.AndroTweet.Contract;

import pt.isel.pdm.AndroTweet.Provider.ContractProvider;
import android.net.Uri;
import android.provider.BaseColumns;

public class StatusContract {

	public static final String TABLE 	= "STATUS";
	public static final Uri CONTENT_URI = 
			Uri.withAppendedPath(ContractProvider.CONTENT_URI, TABLE);
	public static final String 
	  _ID 	= BaseColumns._ID,
	  POST 	= "post";	
}

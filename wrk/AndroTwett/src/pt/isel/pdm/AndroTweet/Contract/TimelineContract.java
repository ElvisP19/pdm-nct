package pt.isel.pdm.AndroTweet.Contract;

import pt.isel.pdm.AndroTweet.Provider.ContractProvider;
import android.net.Uri;
import android.provider.BaseColumns;

public class TimelineContract {

	public static final String TABLE 	= "TIMELINE";
	public static final Uri CONTENT_URI = 
			Uri.withAppendedPath(ContractProvider.CONTENT_URI, TABLE);
	public static final String 
	  _ID 		= BaseColumns._ID,
	  MESSAGE 	= "message",
	  AUTHOR 	= "author",
	  ISNEW		= "isnew",
	  DATE 		= "date";
	 // ID_POST = "idpost";
}
